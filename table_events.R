#' @title create events table
#'
#' @description create a table of events data

dbg$site_id <- 1
ldp$site_id <- 2

events <- bind_rows(
  dbg,
  ldp
  ) %>%
mutate(
  id = seq_along(timestamp),
  site_id = as.integer(site_id),
  replicate = NA_integer_,
  filename = NA_character_
  ) %>%
select(
  id,
  timestamp,
  site_id,
  replicate,
  record,
  filename
)

# construct table and sequence
r_pg_table(
  schema = "micromet",
  table = "events",
  owner = "caplter"
)

# add timestamping to create and update
add_timestamping(
  schema = "micromet",
  table = "events",
)

# modify column types
dbExecute(pg, "
  ALTER TABLE micromet.events
    ALTER COLUMN site_id SET NOT NULL,
    ALTER COLUMN timestamp SET NOT NULL,
    ALTER COLUMN timestamp TYPE TIMESTAMP without time zone USING timestamp::timestamp without time zone ;
  ")

# add foreign key
  dbExecute(pg, "
  ALTER TABLE micromet.events
    ADD CONSTRAINT events_fk_site_id
    FOREIGN KEY (site_id) REFERENCES micromet.sites(id);
    ")

# add unique indices
dbExecute(pg, "CREATE UNIQUE INDEX unique_events_timestamp_site_id_replicate ON micromet.events (site_id, timestamp, replicate) ;")
dbExecute(pg, "ALTER TABLE micromet.events ADD CONSTRAINT unique_events_timestamp_site_id_replicate UNIQUE USING INDEX unique_events_timestamp_site_id_replicate ;")
