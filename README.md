## capmicromet-database

Workflow for transition of the CAP LTER database of micrometeorological data from lter120 to micromet.

### overview

The workflow documented here is to migrate the CAP LTER postgres micromet database (lter120) housed in the GIOS ecosystem to a new postgres (micromet) database in the RC environment. As part of the migration, the database is converted from an almost completely normalized form to a proper, denormalized structure. The workflow is documented in the `database_migration` script, which calls on helper functions that generate and populate the new schema.

![](./assets/figures/capmicromet-database-schema.png)

### miscellaneous

- A revised workflow in the form of the [micromet](https://gitlab.com/caplter/capmicromet) R package has been created for uploading new data to this new database structure.
- The final version of lter120 is archived in the `archive` directory of this repository.
